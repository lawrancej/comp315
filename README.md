# Screen Recording Instructions

Before following lab instructions, please go into this folder.

    cd comp315

Before you start, create a folder for the lab and go into it. For example:

    mkdir lab4
    cd lab4

Then record the screen.
Unix has a command called `script` for this.
Type in the following, where `XX` is the lab number:

    script -f -t 2> lab.timing -a lab.session

When you are done, type `exit` and press Enter.
This finishes recording the screen.

# Screen replay

You can watch what you did earlier. Just type:

    scriptreplay lab.timing lab.session

# Submitting your work
Submit to bitbucket instead of taking screenshots.

    cd
    cd comp315
    git add lab4                  # Add the lab to git
    git commit -m "Lab whatever." # Record to git
    git push me master            # Send to private git repo

